# Lab 1 -- Introduction to the quality gates

[![pipeline status](https://gitlab.com/SmetaninGleb/s23-lab-1-introduction-to-the-quality-gates/badges/main/pipeline.svg)](https://gitlab.com/SmetaninGleb/s23-lab-1-introduction-to-the-quality-gates/-/commits/main)

[![coverage report](https://gitlab.com/SmetaninGleb/s23-lab-1-introduction-to-the-quality-gates/badges/main/coverage.svg)](https://gitlab.com/SmetaninGleb/s23-lab-1-introduction-to-the-quality-gates/-/commits/main)

## Homework

The project gets deployed to Innopolis Virtual Machine. Check out at [10.90.138.252:8080/hello](http://10.90.138.252:8080/hello).
